package gui;

import com.jaimepardo.practica1.*;
import enums.TipoCoche;
import util.Util;

import javax.persistence.PersistenceException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener, ChangeListener {
    private Modelo modelo;
    private Vista vista;
    boolean refrescar;
    static int mecanicos;
    static int recambios;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        mecanicos = 1;
        recambios = 1;
        modelo.conectar(vista);
        addActionListeners(this);
        refrescarTodo();
    }

    /**
     * Refresca las tablas de todas las vistas
     */
    private void refrescarTodo() {
        listarMecanicos(modelo.getMecanicos());
        listarClientes(modelo.getClientes());
        listarCoches(modelo.getCoches());
        listarRecambios(modelo.getRecambio());
        listarFacturas(modelo.getFacturas());
        refrescar = false;
    }

    /**
     * Añade los listeners a los botones y les asigna un nombre
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnCocheAnadir.addActionListener(listener);
        vista.btnCocheAnadir.setActionCommand("btnCocheAnadir");
        vista.btnClienteAnadir.addActionListener(listener);
        vista.btnClienteAnadir.setActionCommand("btnClienteAnadir");
        vista.btnMecanicoAnadir.addActionListener(listener);
        vista.btnMecanicoAnadir.setActionCommand("btnMecanicoAnadir");
        vista.btnRecambioAnadir.addActionListener(listener);
        vista.btnRecambioAnadir.setActionCommand("btnRecambioAnadir");
        vista.btnCocheEliminar.addActionListener(listener);
        vista.btnCocheEliminar.setActionCommand("btnCocheEliminar");
        vista.btnClienteEliminar.addActionListener(listener);
        vista.btnClienteEliminar.setActionCommand("btnClienteEliminar");
        vista.btnMecanicoEliminar.addActionListener(listener);
        vista.btnMecanicoEliminar.setActionCommand("btnMecanicoEliminar");
        vista.btnRecambioModificar.addActionListener(listener);
        vista.btnRecambioModificar.setActionCommand("btnRecambioModificar");
        vista.btnCocheModificar.addActionListener(listener);
        vista.btnCocheModificar.setActionCommand("btnCocheModificar");
        vista.btnClienteModificar.addActionListener(listener);
        vista.btnClienteModificar.setActionCommand("btnClienteModificar");
        vista.btnMecanicoModificar.addActionListener(listener);
        vista.btnMecanicoModificar.setActionCommand("btnMecanicoModificar");
        vista.btnEliminarRecambio.addActionListener(listener);
        vista.btnEliminarRecambio.setActionCommand("btnEliminarRecambio");
        vista.btnAddFactura.addActionListener(listener);
        vista.btnAddFactura.setActionCommand("btnFacturaAnadir");
        vista.btnDelFactura.addActionListener(listener);
        vista.btnDelFactura.setActionCommand("btnFacturaEliminar");
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.setActionCommand("guardarOpciones");
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.addMecanico.addActionListener(listener);
        vista.addMecanico.setActionCommand("addMecanico");
        vista.addRecambio.addActionListener(listener);
        vista.addRecambio.setActionCommand("addRecambio");
        vista.delMecanico.addActionListener(listener);
        vista.delMecanico.setActionCommand("delMecanico");
        vista.delRecambio.addActionListener(listener);
        vista.delRecambio.setActionCommand("delRecambio");
        vista.combustionRadioButton.addActionListener(listener);
        vista.combustionRadioButton.setActionCommand("Combustion");
        vista.electricoRadioButton.addActionListener(listener);
        vista.electricoRadioButton.setActionCommand("Electrico");
        vista.hibridoRadioButton.addActionListener(listener);
        vista.hibridoRadioButton.setActionCommand("Hibrido");
        vista.tabbedPane.addChangeListener(this);

        vista.cochesTabla.addListSelectionListener(this);
        vista.mecanicoTabla.addListSelectionListener(this);
        vista.clienteTabla.addListSelectionListener(this);
        vista.recambioTabla.addListSelectionListener(this);
        vista.recambioCocheTabla.addListSelectionListener(this);
        vista.mecanicoCocheTabla.addListSelectionListener(this);
        vista.tablaCocheFactura.addListSelectionListener(this);
        vista.tablaClienteFactura.addListSelectionListener(this);
        vista.tablaFacturas.addListSelectionListener(this);
    }



    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     *
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.mecanicoTabla) {
                Mecanico mecanicoSeleccion = (Mecanico) vista.mecanicoTabla.getSelectedValue();
                vista.txtNombreMecanico.setText(String.valueOf(mecanicoSeleccion.getNombre()));
                vista.txtApellidoMecanico.setText(String.valueOf(mecanicoSeleccion.getApellidos()));
                vista.txtTelefonoMecanico.setText(String.valueOf(mecanicoSeleccion.getTelefono()));
                refrescarCocheMecanico(mecanicoSeleccion);
                vista.agrandarPantalla();
            } else if (e.getSource() == vista.clienteTabla) {
                Cliente clienteSeleccion = (Cliente) vista.clienteTabla.getSelectedValue();
                vista.txtDni.setText(String.valueOf(clienteSeleccion.getDni()));
                vista.txtNombre.setText(String.valueOf(clienteSeleccion.getNombre()));
                vista.txtApellidos.setText(String.valueOf(clienteSeleccion.getApellidos()));
                vista.txtEmail.setText(String.valueOf(clienteSeleccion.getEmail()));
                vista.txtTelefono.setText(String.valueOf(clienteSeleccion.getTelefono()));
                refrescarClienteCoche(modelo.getCochesCliente(clienteSeleccion));
                vista.agrandarPantalla();
            } else if (e.getSource() == vista.cochesTabla) {
                Coche cocheSeleccion = (Coche) vista.cochesTabla.getSelectedValue();
                asignarTipoCoche(cocheSeleccion.getTipo());
                refrescarRecambioCoche(cocheSeleccion);
                refrescarMecanicoCoche(cocheSeleccion);
                vista.txtMatricula.setText(String.valueOf(cocheSeleccion.getMatricula()));
                vista.comboMarca.setSelectedItem(String.valueOf(cocheSeleccion.getMarca()));
                vista.fecha.setDate(LocalDate.ofEpochDay(cocheSeleccion.getFechaAlta().getDate()));
                vista.comboCliente.setSelectedItem(cocheSeleccion.getCliente());
                vista.tituloMecanicos.setVisible(true);
                vista.panelMecanicos.setVisible(true);
                vista.panelRecambios.setVisible(true);
                vista.tituloRecambios.setVisible(true);
                vista.agrandarPantalla();
            } else if (e.getSource() == vista.recambioTabla) {
                Recambio recambioSeleccionado = (Recambio) vista.recambioTabla.getSelectedValue();
                vista.txtRecambioNombre.setText(String.valueOf(recambioSeleccionado.getComponente()));
                vista.txtRecambioPrecio.setText(String.valueOf(recambioSeleccionado.getPrecio()));
                if (recambioSeleccionado.getCombustion()==1){
                    vista.combustionRadioButton1.setSelected(true);
                }else {
                    vista.combustionRadioButton1.setSelected(false);
                }
                if (recambioSeleccionado.getElectrico()==1){
                    vista.electricoRadioButton1.setSelected(true);
                }else {
                    vista.electricoRadioButton1.setSelected(false);
                }
                refrescarCocheRecambio(recambioSeleccionado);
                vista.agrandarPantalla();
            } else if (e.getSource() == vista.tablaClienteFactura) {
                Cliente clienteSeleccion = (Cliente) vista.tablaClienteFactura.getSelectedValue();
                refrescarClienteCocheFactura(modelo.getCochesCliente(clienteSeleccion));
            } else if (e.getSource() == vista.tablaCocheFactura) {
                Coche cocheSeleccion = (Coche) vista.tablaCocheFactura.getSelectedValue();
                calcularPrecio(cocheSeleccion);
            }
        }
    }


    private void refrescarClienteCoche(ArrayList<Coche> cochesCliente) {

            vista.dtmCocheCliente.clear();
            for(Coche c : cochesCliente){
                vista.dtmCocheCliente.addElement(c);
            }

    }

    private void refrescarClienteCocheFactura(ArrayList<Coche> cochesCliente) {

        vista.dtmCocheFactura.clear();
        for(Coche c : cochesCliente){
            vista.dtmCocheFactura.addElement(c);
        }

    }




    /**
     * Selecciona un radiobutton segun el nombre del parametro
     *
     * @param text el coche seleccionado
     */
    private void asignarTipoCoche(String text) {

        if (text.equals(TipoCoche.COMBUSTIBLE.getValor())){
            vista.combustionRadioButton.setSelected(true);
            listarRecambio(modelo.getRecambio(),true,false);
        }else if (text.equals(TipoCoche.ELECTRICO.getValor())){
            vista.electricoRadioButton.setSelected(true);
            listarRecambio(modelo.getRecambio(),false,true);
        }else {
            vista.hibridoRadioButton.setSelected(false);
            listarRecambio(modelo.getRecambio(),true,true);
        }

    }

    /**
     * Metodo que asigna las acciones a los botones
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Conectar":
                modelo.conectar(vista);
                break;
            case "Desconectar":
                modelo.desconectar();
                vista.panel1.setVisible(false);
                break;
            case "Salir":
                System.exit(0);
                break;
            case "btnCocheAnadir":
                try {
                    if (comprobarCocheVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.cochesTabla.clearSelection();
                    } else if (modelo.cocheMatriculaYaExiste(vista.txtMatricula.getText())) {
                        Util.showErrorAlert("Ese coche ya existe");
                        vista.cochesTabla.clearSelection();
                    } else if (modelo.clienteYaAsignado(String.valueOf(vista.comboCliente.getSelectedItem()))) {
                        Util.showErrorAlert("Ese cliente ya tiene un coche");
                        vista.cochesTabla.clearSelection();
                    } else if (camposRepetidosRecambio() || camposRepetidosMecanico()){
                        Util.showErrorAlert("Existen campos repetidos");
                        vista.cochesTabla.clearSelection();
                    } else {
                        Coche nuevoCoche = new  Coche();
                        nuevoCoche.setTipo(tipoCoche());
                        nuevoCoche.setMatricula(vista.txtMatricula.getText());
                        nuevoCoche.setMarca((String) vista.comboMarca.getSelectedItem());
                        nuevoCoche.setFechaAlta(Date.valueOf(vista.fecha.getDate()));
                        nuevoCoche.setCliente((Cliente) vista.comboCliente.getSelectedItem());
                        modelo.insertarCoche(nuevoCoche);
                        nuevoCoche = modelo.getCocheMatricula(nuevoCoche.getMatricula());
                        insertarRecambios(nuevoCoche);
                        insertarMecanicos(nuevoCoche);
                        borrarcamposCoche();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.cochesTabla.clearSelection();
                } catch (PersistenceException per){
                    Util.showErrorAlert("Matricula ya esta en uso");
                }
                listarCoches(modelo.getCoches());
                break;
            case "btnCocheModificar":
                try {
                    if (comprobarCocheVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.cochesTabla.clearSelection();
                    } else {
                        Coche cocheSeleccion = (Coche)vista.cochesTabla.getSelectedValue();
                        cocheSeleccion.setTipo(tipoCoche());
                        cocheSeleccion.setMatricula(vista.txtMatricula.getText());
                        cocheSeleccion.setMarca((String) vista.comboMarca.getSelectedItem());
                        cocheSeleccion.setFechaAlta(Date.valueOf(vista.fecha.getDate()));
                        cocheSeleccion.setCliente((Cliente) vista.comboCliente.getSelectedItem());
                        retirarRecambios(cocheSeleccion);
                        retirarMecanicos(cocheSeleccion);
                        insertarRecambios(cocheSeleccion);
                        insertarMecanicos(cocheSeleccion);
                        modelo.modificarCoche(cocheSeleccion);
                        refrescarTodo();
                        borrarcamposCoche();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce numeros en los campos que lo requieran");
                    vista.cochesTabla.clearSelection();
                } catch (NullPointerException npe){
                    Util.showErrorAlert("Selecciona un dato");
                    vista.clienteTabla.clearSelection();
                }
                listarCoches(modelo.getCoches());
                break;
            case "btnCocheEliminar":
                try {
                    Coche cocheBorrado  = (Coche)vista.cochesTabla.getSelectedValue();
                    modelo.borrarCoche(cocheBorrado);
                    vista.tituloMecanicos.setVisible(false);
                    vista.panelMecanicos.setVisible(false);
                    vista.panelRecambios.setVisible(false);
                    vista.tituloRecambios.setVisible(false);
                    vista.encogerPantalla();
                    borrarcamposCoche();
                    listarCoches(modelo.getCoches());
                } catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un coche");
                    vista.cochesTabla.clearSelection();
                }

                break;
            case "btnMecanicoAnadir":
                try {
                    if (comprobarMecanicoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.mecanicoTabla.clearSelection();
                    } else {
                            Mecanico nuevoMecanico = new  Mecanico();
                            nuevoMecanico.setNombre(vista.txtNombreMecanico.getText());
                            nuevoMecanico.setApellidos(vista.txtApellidoMecanico.getText());
                            nuevoMecanico.setTelefono(Integer.parseInt(vista.txtTelefonoMecanico.getText()));
                            modelo.insertarMecanico(nuevoMecanico);
                            listarMecanicos(modelo.getMecanicos());
                            borrarCamposMecanico();
                    }
                } catch (Exception ex) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.mecanicoTabla.clearSelection();
                }

                break;
            case "btnMecanicoModificar":
                try {
                    if (comprobarMecanicoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.mecanicoTabla.clearSelection();
                    } else {
                        Mecanico mecanicoSeleccion = (Mecanico)vista.mecanicoTabla.getSelectedValue();
                        mecanicoSeleccion.setNombre(vista.txtNombreMecanico.getText());
                        mecanicoSeleccion.setApellidos(vista.txtApellidoMecanico.getText());
                        mecanicoSeleccion.setTelefono(Integer.parseInt(vista.txtTelefonoMecanico.getText()));
                        modelo.modificarMecanico(mecanicoSeleccion);
                        listarMecanicos(modelo.getMecanicos());
                        borrarCamposMecanico();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.mecanicoTabla.clearSelection();
                } catch (NullPointerException npe){
                    Util.showErrorAlert("Selecciona un dato");
                    vista.clienteTabla.clearSelection();
                }
                borrarCamposMecanico();
                break;
            case "btnMecanicoEliminar":
                try {
                    try{
                        Mecanico mecanicoBorrado  = (Mecanico)vista.mecanicoTabla.getSelectedValue();
                        modelo.borrarMecanico(mecanicoBorrado);
                    }catch (Exception s){
                        Util.showErrorAlert("Ese mecanico tiene un vehiculo asociado");
                    }
                    borrarCamposMecanico();
                    listarMecanicos(modelo.getMecanicos());
                }catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un mecanico");
                    vista.cochesTabla.clearSelection();
                }

                break;
            case "btnClienteAnadir":
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clienteTabla.clearSelection();
                    } else if (modelo.dniClienteYaExiste(vista.txtDni.getText())) {
                        Util.showErrorAlert("Ese DNI ya existe.\nIntroduce un DNI diferente.");
                        vista.clienteTabla.clearSelection();
                    } else {
                        Cliente nuevoCliente = new  Cliente();
                        nuevoCliente.setDni(vista.txtDni.getText());
                        nuevoCliente.setNombre(vista.txtNombre.getText());
                        nuevoCliente.setApellidos(vista.txtApellidos.getText());
                        nuevoCliente.setEmail(vista.txtEmail.getText());
                        nuevoCliente.setTelefono(Integer.parseInt(vista.txtTelefono.getText()));
                        modelo.insertarCliente(nuevoCliente);
                        listarClientes(modelo.getClientes());
                        borrarCamposCliente();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clienteTabla.clearSelection();
                }

                break;
            case "btnClienteModificar":
                try {
                    if (comprobarClienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.clienteTabla.clearSelection();
                    } else {
                        Cliente clienteSeleccion = (Cliente)vista.clienteTabla.getSelectedValue();
                        clienteSeleccion.setDni(vista.txtDni.getText());
                        clienteSeleccion.setNombre(vista.txtNombre.getText());
                        clienteSeleccion.setApellidos(vista.txtApellidos.getText());
                        clienteSeleccion.setEmail(vista.txtEmail.getText());
                        clienteSeleccion.setTelefono(Integer.parseInt(vista.txtTelefono.getText()));
                        modelo.modificarCliente(clienteSeleccion);
                        listarClientes(modelo.getClientes());
                        borrarCamposCliente();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.clienteTabla.clearSelection();
                } catch (NullPointerException npe){
                    Util.showErrorAlert("Selecciona un dato");
                    vista.clienteTabla.clearSelection();
                }

                break;
            case "btnClienteEliminar":
                try {
                    try {
                        Cliente clienteBorrado  = (Cliente)vista.clienteTabla.getSelectedValue();
                        modelo.borrarCliente(clienteBorrado);
                    }catch (Exception s){
                        Util.showErrorAlert("Ese cliente tiene un vehiculo asociado");
                    }
                }catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un cliente");
                    vista.cochesTabla.clearSelection();
                }
                borrarCamposCliente();
                listarClientes(modelo.getClientes());
                break;
            case "btnRecambioAnadir":
                try {
                    if (comprobarRecambioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.recambioTabla.clearSelection();
                    } else {
                        try {
                            Recambio nuevoRecambio = new Recambio();
                            nuevoRecambio.setComponente(vista.txtRecambioNombre.getText());
                            nuevoRecambio.setPrecio(Double.parseDouble(vista.txtRecambioPrecio.getText()));
                            if (vista.combustionRadioButton1.isSelected()){
                                nuevoRecambio.setCombustion((byte) 1);
                            }else {
                                nuevoRecambio.setCombustion((byte) 0);
                            }
                            if (vista.electricoRadioButton1.isSelected()){
                                nuevoRecambio.setElectrico((byte) 1);
                            }else {
                                nuevoRecambio.setElectrico((byte) 0);
                            }
                            modelo.insertarRecambio(nuevoRecambio);
                            listarRecambios(modelo.getRecambio());
                            borrarCamposRecambio();
                        }catch (Exception s){
                            Util.showErrorAlert("Valor introducido incorrecto");
                        }

                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.recambioTabla.clearSelection();
                }

                break;
            case "btnRecambioModificar":
                try {
                    if (comprobarRecambioVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.recambioTabla.clearSelection();
                    } else {
                        Recambio recambioSeleccion = (Recambio)vista.recambioTabla.getSelectedValue();
                        recambioSeleccion.setComponente(vista.txtRecambioNombre.getText());
                        recambioSeleccion.setPrecio(Double.parseDouble(vista.txtRecambioPrecio.getText()));
                        if (vista.combustionRadioButton1.isSelected()){
                            recambioSeleccion.setCombustion((byte) 1);
                        }else {
                            recambioSeleccion.setCombustion((byte) 0);
                        }
                        if (vista.electricoRadioButton1.isSelected()){
                            recambioSeleccion.setElectrico((byte) 1);
                        }else {
                            recambioSeleccion.setElectrico((byte) 0);
                        }
                        modelo.modificarRecambio(recambioSeleccion);
                        listarRecambios(modelo.getRecambio());
                        borrarCamposRecambio();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.recambioTabla.clearSelection();
                } catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Ningun elemento seleccionado");
                    vista.recambioTabla.clearSelection();
                }
                borrarCamposCliente();
                break;
            case "btnEliminarRecambio":
                try {
                    try {
                        Recambio recambioBorrado  = (Recambio)vista.recambioTabla.getSelectedValue();
                        modelo.borrarRecambio(recambioBorrado);
                        refrescarTodo();
                    }catch (Exception s){
                        Util.showErrorAlert("El recambio esta siendo usado en un coche");
                    }

                    borrarCamposRecambio();
                    listarRecambios(modelo.getRecambio());
                }catch (ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert("Selecciona un recambio");
                    vista.cochesTabla.clearSelection();
                }

                break;
            case "btnFacturaAnadir":
                if (!vista.tablaClienteFactura.isSelectionEmpty() && !vista.tablaCocheFactura.isSelectionEmpty()){
                    Factura nuevaFactura = new Factura();
                    nuevaFactura.setCliente((Cliente) vista.tablaClienteFactura.getSelectedValue());
                    nuevaFactura.setCoche(((Coche) vista.tablaCocheFactura.getSelectedValue()).getMarca());
                    nuevaFactura.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                    modelo.insertarFactura(nuevaFactura);
                    refrescarTodo();
                }else {
                    Util.showErrorAlert("Selecciona los datos");
                }
                break;
            case "btnFacturaEliminar":
                if (!vista.tablaFacturas.isSelectionEmpty()){
                    Factura factura  = (Factura) vista.tablaFacturas.getSelectedValue();
                    modelo.borrarFactura(factura);
                    refrescarTodo();
                }else {
                    Util.showErrorAlert("Selecciona un dato");
                }
                break;

            case "addRecambio":
                addRecambio();
                break;
            case "delRecambio":
                delRecambio();
                break;
            case "addMecanico":
                addMecanico();
                break;
            case "delMecanico":
                delMecanico();
                break;
            case "Combustion":
                listarRecambio(modelo.getRecambio(),true,false);
                break;
            case "Electrico":
                listarRecambio(modelo.getRecambio(),false,true);
                break;
            case "Hibrido":
                listarRecambio(modelo.getRecambio(),true,true);
                break;
        }
    }

    /**
     * Inserta los mecanicos de los combobox en el coche
     *
     * @param coche coche que se desea añadir los mecanicos
     */
    private void insertarMecanicos(Coche coche) {

        for (int i=0;i<mecanicos;i++){

            Mecanico mecanico = (Mecanico) vista.cbMecanico.get(i).getSelectedItem();
            modelo.insertarMecanicoCoche(coche,mecanico);

        }


    }

    /**
     * Inserta los recambios de los combobox en el coche
     *
     * @param coche coche que se desea añadir los recambios
     */
    private void insertarRecambios(Coche coche) {

        for (int i=0;i<recambios;i++){

            Recambio recambio = (Recambio) vista.cbRecambio.get(i).getSelectedItem();
            modelo.insertarRecambiosCoche(coche,recambio);

        }

    }

    /**
     * Retira los recambios de los combobox en el coche
     *
     * @param coche coche que se desea Retira los recambios
     */
    private void retirarRecambios(Coche coche) {

        for (RecambioCoche r: coche.getRecambioscoche()) {
                r.setCoche(null);
                r.setRecambio(null);
        }

    }

    /**
     * Retira los recambios de los combobox en el coche
     *
     * @param coche coche que se desea Retira los recambios
     */
    private void retirarMecanicos(Coche coche) {

        for (MecanicoCoche r: coche.getMecanicoscoche()) {
            r.setCoche(null);
            r.setMecanico(null);
        }

    }

    /**
     * Devuelve un String con el tipo de coche segun el radiobutton seleccionado
     *
     * @return String tipo de coche
     */
    private String tipoCoche() {

        if (vista.combustionRadioButton.isSelected()){
            return TipoCoche.COMBUSTIBLE.getValor();
        }
        else if (vista.electricoRadioButton.isSelected()){
            return TipoCoche.ELECTRICO.getValor();
        }
        else if (vista.hibridoRadioButton.isSelected()){
            return TipoCoche.HIBRIDO.getValor();
        }

        return "";

    }

    /**
     * Actualiza los clientes que se ven en la lista y los comboboxes
     */
    private void listarClientes(ArrayList<Cliente> clientes) {

        vista.dtmClientes.clear();
        vista.dtmClienteFactura.clear();
        vista.comboCliente.removeAllItems();
        for(Cliente cliente : clientes){
            vista.dtmClientes.addElement(cliente);
            vista.dtmClienteFactura.addElement(cliente);
            vista.comboCliente.addItem(cliente);
        }


    }

    /**
     * Actualiza los mecanicos que se ven en la lista y los comboboxes
     */
    private void listarMecanicos(ArrayList<Mecanico> mecanicos) {

        vista.dtmMecanicos.clear();
        for(Mecanico mecanico : mecanicos){
            vista.dtmMecanicos.addElement(mecanico);
        }

        for (int i = 0; i < vista.cbMecanico.size();i++ ){
            vista.cbMecanico.get(i).removeAllItems();
            for(Mecanico mecanico : mecanicos){
                vista.cbMecanico.get(i).addItem(mecanico);
            }
        }

    }


    /**
     * Actualiza los coches que se ven en la lista y los comboboxes
     */
    private void listarCoches(ArrayList<Coche> coches) {

        vista.dtmCoches.clear();
        for(Coche coche : coches){
            vista.dtmCoches.addElement(coche);
        }

    }

    /**
     * Actualiza los recambios que se ven en la lista y los comboboxes
     */
    private void listarRecambios(ArrayList<Recambio> recambios) {

        vista.dtmRecambios.clear();
        for(Recambio recambio : recambios){
            vista.dtmRecambios.addElement(recambio);
        }

    }

    /**
     * Actualiza los recambios que se ven en la lista y los comboboxes
     */
    private void listarFacturas(ArrayList<Factura> facturas) {

        vista.dtmFactura.clear();
        for(Factura factura : facturas){
            vista.dtmFactura.addElement(factura);
        }

    }




    /**
     * Actualiza los recambios que se ven en la lista y los comboboxes
     */
    private void listarRecambio(ArrayList<Recambio> recambios, boolean combustion, boolean electrico) {

        byte c=0;
        byte e=0;

        if (combustion)
            c=1;
        if (electrico)
            e=1;

        for (int i = 0; i < vista.cbRecambio.size();i++ ){
            vista.cbRecambio.get(i).removeAllItems();
            for(Recambio recambio : recambios){
                if (recambio.getCombustion()==c && recambio.getElectrico()==e){
                    vista.cbRecambio.get(i).addItem(recambio);
                }
            }
        }

    }


    /**
     * Refresca los recambio de un coche
     *
     * @param coche id del coche
     */
    private void refrescarRecambioCoche(Coche coche) {

        vista.dtmRecambioCoche.clear();
        for (RecambioCoche recambioCoche: coche.getRecambioscoche()) {
            vista.dtmRecambioCoche.addElement(recambioCoche.getRecambio());
        }

    }


    /**
     * Refresca la tabla de mecanicos de un coche
     *
     * @param coche
     */
    private void refrescarMecanicoCoche(Coche coche) {

        vista.dtmMecanicoCoche.clear();
        for(MecanicoCoche mecanicoCoche : coche.getMecanicoscoche()){
            vista.dtmMecanicoCoche.addElement(mecanicoCoche.getMecanico());
        }

    }

    private void refrescarCocheMecanico(Mecanico mecanico) {

        vista.dtmCocheMecanico.clear();
        for(MecanicoCoche m : mecanico.getMecanicosCoche()){
            vista.dtmCocheMecanico.addElement(m.getCoche());
        }

    }

    private void refrescarCocheRecambio(Recambio recambio) {

        vista.dtmCocheRecambio.clear();
        for(RecambioCoche r : recambio.getRecambiosCoche()){
            vista.dtmCocheRecambio.addElement(r.getCoche());
        }

    }



    /**
     * Borra todos los campos del coche
     */
    private void borrarcamposCoche() {
        vista.comboCliente.setSelectedIndex(-1);
        for (int i = 0;i<vista.cbMecanico.size();i++){
            vista.cbMecanico.get(i).setSelectedIndex(-1);
        }
        for (int i = 0;i<vista.cbRecambio.size();i++){
            vista.cbRecambio.get(i).setSelectedIndex(-1);
        }
        vista.txtMatricula.setText("");
        vista.fecha.clear();
    }

    /**
     * Borra todos los campos del cliente
     */
    private void borrarCamposCliente() {
        vista.txtNombre.setText("");
        vista.txtDni.setText("");
        vista.txtApellidos.setText("");
        vista.txtEmail.setText("");
        vista.txtTelefono.setText("");
    }

    /**
     * Borrar todos los campos del mecanico
     */
    private void borrarCamposMecanico() {
        vista.txtNombreMecanico.setText("");
        vista.txtApellidoMecanico.setText("");
        vista.txtTelefonoMecanico.setText("");
    }

    /**
     * Borrar todos los campos de recambio
     */
    private void borrarCamposRecambio() {
        vista.txtRecambioPrecio.setText("");
        vista.txtRecambioNombre.setText("");
        vista.combustionRadioButton1.setSelected(false);
        vista.electricoRadioButton1.setSelected(false);
    }

    /**
     * Comprueba si los datos de coche estan vacios
     *
     * @return
     */
    private boolean comprobarCocheVacio() {

        boolean cbMecanicoVacio = true;
        for (int i = 0;i<mecanicos;i++){
            cbMecanicoVacio = vista.cbMecanico.get(i).getSelectedIndex() == -1;
        }
        boolean cbRecambioVacio = true;
        for (int i = 0;i<recambios;i++){
            cbRecambioVacio = vista.cbRecambio.get(i).getSelectedIndex() == -1;
        }

        return vista.txtMatricula.getText().isEmpty() ||
                cbMecanicoVacio ||
                cbRecambioVacio ||
                vista.comboMarca.getSelectedIndex() == -1||
                vista.comboCliente.getSelectedIndex() == -1;
    }

    /**
     * Comprueba si los datos de cliente estan vacios
     *
     * @return
     */
    private boolean comprobarClienteVacio() {
        return vista.txtDni.getText().isEmpty() ||
                vista.txtNombre.getText().isEmpty() ||
                vista.txtApellidos.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.txtEmail.getText().isEmpty();
    }

    /**
     * Comprueba si los datos de mecanico estan vacios
     *
     * @return
     */
    private boolean comprobarMecanicoVacio() {
        return vista.txtNombreMecanico.getText().isEmpty() ||
                vista.txtApellidoMecanico.getText().isEmpty() ||
                vista.txtTelefonoMecanico.getText().isEmpty();
    }

    /**
     * Comprueba si los datos de recambio estan vacios
     *
     * @return boolean de si esta vacio o no
     */
    private boolean comprobarRecambioVacio() {

        if (!vista.combustionRadioButton1.isSelected() && !vista.electricoRadioButton1.isSelected()){
            return true;
        }

        if (vista.txtRecambioNombre.getText().isEmpty() || vista.txtRecambioPrecio.getText().isEmpty()){
            return true;
        }

        return false;

    }

    /**
     * Comprueba si los datos de mecanicos estan vacios
     *
     * @return boolean de si esta vacio o no
     */
    private boolean camposRepetidosMecanico() {

        ArrayList<Mecanico> list = new ArrayList<>();

        for (int i = 0;i<mecanicos;i++){
            list.add((Mecanico) vista.cbMecanico.get(i).getSelectedItem());
        }

        for (int i = 1; i<list.size();i++){
            if (list.get(i) == list.get(i-1))
                return true;
        }

        return false;

    }

    /**
     * Comprueba si los datos de recambio estan vacios
     *
     * @return boolean de si esta vacio o no
     */
    private boolean camposRepetidosRecambio() {

        ArrayList<Recambio> list = new ArrayList<>();

        for (int i = 0;i<recambios;i++){
            list.add((Recambio) vista.cbRecambio.get(i).getSelectedItem());
        }

        for (int i = 1; i<list.size();i++){
            if (list.get(i) == list.get(i-1))
                return true;
        }

        return false;

    }


    /**
     * Borra un CB de mecanico en el coche
     */
    private void delMecanico() {

        for (int i=vista.cbMecanico.size()-1;i>=0;i--){

            if (vista.cbMecanico.get(i).isVisible() && i!=0){
                vista.cbMecanico.get(i).setVisible(false);
                vista.cbMecanico.get(i).setSelectedIndex(-1);
                vista.cbtMecanico.get(i).setVisible(false);
                mecanicos--;
                return;
            }
        }
    }

    /**
     * Añade un CB de mecanico en el coche
     */
    private void addMecanico() {

        for (int i=0;i<vista.cbMecanico.size();i++){

            if (!vista.cbMecanico.get(i).isVisible() && i!=0){
                vista.cbMecanico.get(i).setVisible(true);
                vista.cbtMecanico.get(i).setVisible(true);
                mecanicos++;
                return;
            }
        }

    }

    /**
     * Borra un CB de recambio en el coche
     */
    private void delRecambio() {

        for (int i=vista.cbRecambio.size()-1;i>=0;i--){

            if (vista.cbRecambio.get(i).isVisible() && i!=0){
                vista.cbRecambio.get(i).setVisible(false);
                vista.cbRecambio.get(i).setSelectedIndex(-1);
                vista.cbtRecambio.get(i).setVisible(false);
                recambios--;
                return;
            }
        }

    }

    /**
     * Añade un CB de recambio en el coche
     */
    private void addRecambio() {

        for (int i=0;i<vista.cbRecambio.size();i++){

            if (!vista.cbRecambio.get(i).isVisible() && i!=0){
                vista.cbRecambio.get(i).setVisible(true);
                vista.cbtRecambio.get(i).setVisible(true);
                recambios++;
                return;
            }
        }

    }

    /**
     * Calcula el precio de los recambios de un coche
     *
     * @param cocheSeleccion
     */
    private void calcularPrecio(Coche cocheSeleccion) {

        double precio = 0;

        for(RecambioCoche r : modelo.getCochesFactura(cocheSeleccion)){
            precio += r.getRecambio().getPrecio();
        }

        vista.txtPrecio.setText(precio+"");

    }


    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Metodo que se ejecuta cada vez que se cambia de pestaña
     *
     * @param e
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        refrescarTodo();
        vista.tituloMecanicos.setVisible(false);
        vista.panelMecanicos.setVisible(false);
        vista.panelRecambios.setVisible(false);
        vista.tituloRecambios.setVisible(false);
        vista.encogerPantalla();
    }
}
