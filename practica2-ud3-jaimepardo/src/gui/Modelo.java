package gui;

import com.jaimepardo.practica1.*;
import org.hibernate.Session;
import util.HibernateUtil;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Jaime Pardo on 13/12/2021.
 */
public class Modelo {

    /**
     * Metodo que conecta con la base de datos
     *
     * @param vista
     */
    void conectar(Vista vista) {

        HibernateUtil.buildSessionFactory(vista);
        HibernateUtil.openSession();

        }

    /**
     * Metodo que lee el fichero SQL
     *
     * @return
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        //basedatos_java no tiene delimitador
        //StringBuilder es dinamica
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
    }

    /**
     * Metodo que desconecta la base
     */
    void desconectar() {
        HibernateUtil.closeSessionFactory();
    }

    /**
     * Metodo que inserta Cliente
     */
    void insertarCliente(Cliente cliente) {

        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(cliente);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta Mecanico
     */
    void insertarMecanico(Mecanico mecanico) {

        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(mecanico);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que relaciona un recambio
     **/
    public void insertarRecambiosCoche(Coche coche, Recambio recambio) {

        RecambioCoche recambioCoche = new RecambioCoche();
        recambioCoche.setRecambio(recambio);
        recambioCoche.setCoche(coche);
        recambioCoche.setCantidad(recambioCoche.getCantidad()+1);

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(recambioCoche);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que relaciona un recambio
     **/
    public void insertarMecanicoCoche(Coche coche, Mecanico mecanico) {

        MecanicoCoche mecanicoCoche = new MecanicoCoche();
        mecanicoCoche.setMecanico(mecanico);
        mecanicoCoche.setCoche(coche);
        mecanicoCoche.setCantidad(mecanicoCoche.getCantidad()+1);

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(mecanicoCoche);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta Coche
     */
    void insertarCoche(Coche coche) throws PersistenceException {

        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(coche);
        sesion.getTransaction().commit();

    }

    /**
     * Metodo que inserta un recambio
     */
    public void insertarRecambio(Recambio recambio)  {

        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(recambio);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que inserta una factura
     */
    public void insertarFactura(Factura factura)  {

        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(factura);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que modifica un cliente
     */
    void modificarCliente(Cliente cliente) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(cliente);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que modifica un mecanico
     */
    void modificarMecanico(Mecanico mecanico) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(mecanico);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que modifica un coche
     */
    void modificarCoche(Coche coche) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(coche);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que modifica un recambio
     */
    public void modificarRecambio(Recambio recambio) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(recambio);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un mecanico
     */
    void borrarMecanico(Mecanico mecanico) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(mecanico);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un cliente
     */
    void borrarCliente(Cliente cliente)  {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(cliente);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un coche
     */
    void borrarCoche(Coche coche) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.getTransaction().begin();
        sesion.delete(coche);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra un recambio
     */
    void borrarRecambio(Recambio recambio) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(recambio);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Metodo que borra una factura
     */
    void borrarFactura(Factura factura) {

        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(factura);
        sesion.getTransaction().commit();
        sesion.close();

    }

    /**
     * Consulta los campos de mecanicos
     *
     * @return Array de los valores obtenidos
     * @throws SQLException
     */
    public ArrayList<Mecanico> getMecanicos() {
        return (ArrayList<Mecanico>) HibernateUtil.getCurrentSession().createQuery("FROM Mecanico").getResultList();
    }

    /**
     * Consulta los campos de coche
     *
     * @return Array de los valores obtenidos
     * @throws SQLException
     */
    public ArrayList<Coche> getCoches() {
        return (ArrayList<Coche>) HibernateUtil.getCurrentSession().createQuery("FROM Coche").getResultList();
    }

    /**
     * Consulta los campos de cliente
     *
     * @return Array de los valores obtenidos
     * @throws SQLException
     */
    public ArrayList<Cliente> getClientes() {
        return (ArrayList<Cliente>) HibernateUtil.getCurrentSession().createQuery("FROM Cliente").getResultList();
    }

    /**
     * Consulta los campos de facturas
     *
     * @return Array de los valores obtenidos
     * @throws SQLException
     */
    public ArrayList<Factura> getFacturas() {
        return (ArrayList<Factura>) HibernateUtil.getCurrentSession().createQuery("FROM Factura").getResultList();
    }

    /**
     * Consulta los campos de recambio
     *
     * @return Array de los valores obtenidos
     * @throws SQLException
     */
    public ArrayList<Recambio> getRecambio() {
        return (ArrayList<Recambio>) HibernateUtil.getCurrentSession().createQuery("FROM Recambio").getResultList();
    }

    public ArrayList<Recambio> getRecambioElectrico() {
        return (ArrayList<Recambio>) HibernateUtil.getCurrentSession().createQuery("FROM Recambio WHERE electrico=1").getResultList();
    }

    public ArrayList<Recambio> getRecambioCombustion() {
        return (ArrayList<Recambio>) HibernateUtil.getCurrentSession().createQuery("FROM Recambio WHERE combustion=1").getResultList();
    }

    public ArrayList<Recambio> getRecambioHibrido() {
        return (ArrayList<Recambio>) HibernateUtil.getCurrentSession().createQuery("FROM Recambio WHERE electrico=1 OR combustion=1").getResultList();
    }


    /**
     * Comprueba si la matricula ya existe
     *
     * @param matricula matricula a comprobar
     * @return
     */
    public boolean cocheMatriculaYaExiste(String matricula) {
        return false;
    }

    /**
     * Comrpueba que el dni del cliente ya existe
     *
     * @param dni DNI a comprobar
     * @return
     */
    public boolean dniClienteYaExiste(String dni) {
        return false;
    }

    /**
     * Comprueba si el cliente ya tiene un coche asignado
     *
     * @param cliente Cliente a comprobar
     * @return
     */
    public boolean clienteYaAsignado(String cliente) {
        return false;
    }


    /**
     * Devuelve una lista de coches de un cliente
     *
     * @param c Cliente
     * @return
     */
    public ArrayList<Coche> getCochesCliente(Cliente c) {
        Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM Coche WHERE id_cliente = :c");
        query.setParameter("c", c.getId());
        ArrayList<Coche> lista = (ArrayList<Coche>) query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     *
     * Devuelve un coche con una matricula
     *
     * @param matricula
     * @return
     */
    public Coche getCocheMatricula(String matricula) {
        Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM Coche WHERE matricula = :m");
        query.setParameter("m", matricula);
        ArrayList<Coche> lista = (ArrayList<Coche>) query.getResultList();
        sesion.close();
        return lista.get(0);
    }

    /**
     * Devuelve una lista de recambios de un coche
     *
     * @param coche
     * @return
     */
    public ArrayList<RecambioCoche> getCochesFactura(Coche coche) {
        Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM RecambioCoche WHERE coche = :c");
        query.setParameter("c", coche);
        ArrayList<RecambioCoche> lista = (ArrayList<RecambioCoche>) query.getResultList();
        sesion.close();
        return lista;
    }

}
