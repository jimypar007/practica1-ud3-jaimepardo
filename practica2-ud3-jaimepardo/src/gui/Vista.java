package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.Marcas;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Clase que crea el frame de Vista
 */
public class Vista extends JFrame {
    private final static String TITULOFRAME="Aplicación Recambios Jaime Pardo";
    JTabbedPane tabbedPane;
    public JPanel panel1;
    JPanel JPanelCoche;
    JPanel JPanelCliente;
    JPanel JPanelMecanico;
    JPanel JPanelRecambio;

    //Coches
    JRadioButton hibridoRadioButton;
    JRadioButton electricoRadioButton;
    JRadioButton combustionRadioButton;
    JTextField txtMatricula;
    DatePicker fecha;
    JButton addRecambio;
    JButton delRecambio;
    JButton addMecanico;
    JButton delMecanico;
    JComboBox comboCliente;
    ArrayList<JLabel> cbtRecambio;
    JLabel txtRecambio1;
    JLabel txtRecambio2;
    JLabel txtRecambio3;
    JLabel txtRecambio4;
    JLabel txtRecambio5;
    ArrayList<JComboBox> cbRecambio;
    JComboBox comboRecambio1;
    JComboBox comboRecambio2;
    JComboBox comboRecambio3;
    JComboBox comboRecambio4;
    JComboBox comboRecambio5;
    JButton btnCocheAnadir;
    JButton btnCocheModificar;
    JButton btnCocheEliminar;
    JList cochesTabla;
    JList mecanicoCocheTabla;
    JList recambioCocheTabla;


    //CLIENTES
    JTextField txtDni;
    JTextField txtEmail;
    JTextField txtNombre;
    JTextField txtApellidos;
    JTextField txtTelefono;
    JList clienteTabla;
    JButton btnClienteEliminar;
    JButton btnClienteAnadir;
    JButton btnClienteModificar;

    //MECANICOS
    JTextField txtNombreMecanico;
    JTextField txtApellidoMecanico;
    JTextField txtTelefonoMecanico;
    JList mecanicoTabla;
    JButton btnMecanicoEliminar;
    JButton btnMecanicoAnadir;
    JButton btnMecanicoModificar;
    ArrayList<JLabel> cbtMecanico;
    JLabel txtMecanico1;
    JLabel txtMecanico2;
    JLabel txtMecanico3;
    JLabel txtMecanico4;
    JLabel txtMecanico5;
    ArrayList<JComboBox> cbMecanico;
    JComboBox comboMecanico1;
    JComboBox comboMecanico2;
    JComboBox comboMecanico3;
    JComboBox comboMecanico4;
    JComboBox comboMecanico5;
    JComboBox comboMarca;

    //Recambios

    JRadioButton combustionRadioButton1;
    JRadioButton electricoRadioButton1;
    JTextField txtRecambioNombre;
    JTextField txtRecambioPrecio;
    JList recambioTabla;
    JButton btnRecambioAnadir;
    JButton btnRecambioModificar;
    JButton btnEliminarRecambio;


    JScrollPane panelMecanicos;
    JScrollPane panelRecambios;
    JLabel tituloMecanicos;
    JLabel tituloRecambios;
    JList cocheClienteTabla;
    JList cocheRecambioTabla;
    JLabel Coches;
    JList cocheMecanicoTabla;
    JList tablaClienteFactura;
    JList tablaCocheFactura;
    JList tablaFacturas;
    JLabel txtPrecio;
    JButton btnAddFactura;
    JButton btnDelFactura;

    //BUSQUEDA
    JLabel etiquetaEstado;

    //DEFAULT TABLE MODEL
    DefaultListModel dtmCoches;
    DefaultListModel dtmClientes;
    DefaultListModel dtmMecanicos;
    DefaultListModel dtmRecambios;
    DefaultListModel dtmRecambioCoche;
    DefaultListModel dtmMecanicoCoche;
    DefaultListModel dtmCocheCliente;
    DefaultListModel dtmCocheRecambio;
    DefaultListModel dtmCocheMecanico;
    DefaultListModel dtmCocheFactura;
    DefaultListModel dtmClienteFactura;
    DefaultListModel dtmFactura;

    //BARRA MENU
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    //CUADRO DIALOGO
    OptionDialog optionDialog;
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    public Vista() {
        initFrame();
    }

    /**
     * Inicia los componentes del frames
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setSize(900,500);
        this.setVisible(true);
        //creo cuadro de dialogo
        optionDialog=new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setComboBox();
        setEnumComboBox();
        setTableModels();

        this.panel1.setVisible(false);
    }

    /**
     * Asigna los table models a las tablas de la vista
     */
    private void setTableModels() {
        this.dtmCoches=new DefaultListModel();
        this.cochesTabla.setModel(dtmCoches);
        this.dtmMecanicos=new DefaultListModel();
        this.mecanicoTabla.setModel(dtmMecanicos);
        this.dtmClientes=new DefaultListModel();
        this.clienteTabla.setModel(dtmClientes);
        this.dtmRecambios=new DefaultListModel();
        this.recambioTabla.setModel(dtmRecambios);
        this.recambioCocheTabla.setEnabled(false);
        this.dtmRecambioCoche = new DefaultListModel();
        this.recambioCocheTabla.setModel(dtmRecambioCoche);
        this.mecanicoCocheTabla.setEnabled(false);
        this.dtmMecanicoCoche = new DefaultListModel();
        this.mecanicoCocheTabla.setModel(dtmMecanicoCoche);
        this.cocheClienteTabla.setEnabled(false);
        this.dtmCocheCliente = new DefaultListModel();
        this.cocheClienteTabla.setModel(dtmCocheCliente);
        this.dtmCocheMecanico = new DefaultListModel();
        this.cocheMecanicoTabla.setModel(dtmCocheMecanico);
        this.cocheMecanicoTabla.setEnabled(false);
        this.dtmCocheRecambio = new DefaultListModel();
        this.cocheRecambioTabla.setModel(dtmCocheRecambio);
        this.cocheRecambioTabla.setEnabled(false);
        this.dtmClienteFactura = new DefaultListModel();
        this.tablaClienteFactura.setModel(dtmClienteFactura);
        this.dtmCocheFactura = new DefaultListModel();
        this.tablaCocheFactura.setModel(dtmCocheFactura);
        this.dtmFactura = new DefaultListModel();
        this.tablaFacturas.setModel(dtmFactura);
    }

    /**
     * Asigna los componentes del menu de opcioness
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        //por cada item que tenga funcionalidad tiene un ActionCommand
        itemOpciones= new JMenuItem("Conectar");
        itemOpciones.setActionCommand("Conectar");
        itemDesconectar= new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        //centrar en horizontal
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Asigna los enumerables al combobox.
     */
    private void setEnumComboBox() {
        for (Marcas m: Marcas.values()) {
            comboMarca.addItem(m.getValor());
        }
        comboMarca.setSelectedIndex(-1);
    }

    /**
     * Asigna los componentes del panel de contraseña
     */
    private void setAdminDialog() {
        btnValidate= new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword= new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(140,26));
        Object[] options = new Object[] {adminPassword,btnValidate};
        JOptionPane jop =new JOptionPane("Introduce la contraseña",
                JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,options);
        adminPasswordDialog=new JDialog(this,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    /**
     * Crea un array de combobox de recambios y mecanicos
     */
    private void setComboBox() {

        cbtRecambio = new ArrayList<>();
        cbtRecambio.add(txtRecambio1);
        cbtRecambio.add(txtRecambio2);
        cbtRecambio.add(txtRecambio3);
        cbtRecambio.add(txtRecambio4);
        cbtRecambio.add(txtRecambio5);
        cbRecambio = new ArrayList<>();
        cbRecambio.add(comboRecambio1);
        cbRecambio.add(comboRecambio2);
        cbRecambio.add(comboRecambio3);
        cbRecambio.add(comboRecambio4);
        cbRecambio.add(comboRecambio5);

        cbtMecanico = new ArrayList<>();
        cbtMecanico.add(txtMecanico1);
        cbtMecanico.add(txtMecanico2);
        cbtMecanico.add(txtMecanico3);
        cbtMecanico.add(txtMecanico4);
        cbtMecanico.add(txtMecanico5);
        cbMecanico = new ArrayList<>();
        cbMecanico.add(comboMecanico1);
        cbMecanico.add(comboMecanico2);
        cbMecanico.add(comboMecanico3);
        cbMecanico.add(comboMecanico4);
        cbMecanico.add(comboMecanico5);

    }


    public void agrandarPantalla(){
        this.setSize(1180,500);
    }

    public void encogerPantalla() {
        this.setSize(900,500);
    }

}
