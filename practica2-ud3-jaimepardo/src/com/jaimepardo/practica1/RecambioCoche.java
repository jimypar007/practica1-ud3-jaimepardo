package com.jaimepardo.practica1;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "recambio_coche", schema = "tallerjaimepardo", catalog = "")
public class RecambioCoche {
    private int id;
    private int cantidad;
    private Coche coche;
    private Recambio recambio;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecambioCoche that = (RecambioCoche) o;
        return id == that.id &&
                cantidad == that.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }

    @ManyToOne
    @JoinColumn(name = "id_coche", referencedColumnName = "id")
    public Coche getCoche() {
        return coche;
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }

    @ManyToOne
    @JoinColumn(name = "id_recambio", referencedColumnName = "id")
    public Recambio getRecambio() {
        return recambio;
    }

    public void setRecambio(Recambio recambio) {
        this.recambio = recambio;
    }
}
