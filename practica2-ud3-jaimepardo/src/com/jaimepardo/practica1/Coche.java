package com.jaimepardo.practica1;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Coche {
    private int id;
    private String tipo;
    private String matricula;
    private String marca;
    private Date fechaAlta;
    private Cliente cliente;
    private List<RecambioCoche> recambioscoche;
    private List<MecanicoCoche> mecanicoscoche;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "matricula")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "fecha_alta")
    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coche coche = (Coche) o;
        return id == coche.id &&
                Objects.equals(tipo, coche.tipo) &&
                Objects.equals(matricula, coche.matricula) &&
                Objects.equals(marca, coche.marca) &&
                Objects.equals(fechaAlta, coche.fechaAlta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tipo, matricula, marca, fechaAlta);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @OneToMany(mappedBy = "coche")
    public List<RecambioCoche> getRecambioscoche() {
        return recambioscoche;
    }

    public void setRecambioscoche(List<RecambioCoche> recambioscoche) {
        this.recambioscoche = recambioscoche;
    }

    @OneToMany(mappedBy = "coche")
    public List<MecanicoCoche> getMecanicoscoche() {
        return mecanicoscoche;
    }

    public void setMecanicoscoche(List<MecanicoCoche> mecanicoscoche) {
        this.mecanicoscoche = mecanicoscoche;
    }

    @Override
    public String toString() {
        return "["+matricula+"]"+" "+marca+" , "+tipo;
    }
}
