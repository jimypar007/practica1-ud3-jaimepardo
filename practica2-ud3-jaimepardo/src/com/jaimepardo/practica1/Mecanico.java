package com.jaimepardo.practica1;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Mecanico {
    private int id;
    private String nombre;
    private String apellidos;
    private int telefono;
    private List<MecanicoCoche> mecanicosCoche;

    public Mecanico() {
        this.mecanicosCoche = new ArrayList<>();
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mecanico mecanico = (Mecanico) o;
        return id == mecanico.id &&
                telefono == mecanico.telefono &&
                Objects.equals(nombre, mecanico.nombre) &&
                Objects.equals(apellidos, mecanico.apellidos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, telefono);
    }

    @OneToMany(mappedBy = "mecanico")
    public List<MecanicoCoche> getMecanicosCoche() {
        return mecanicosCoche;
    }

    public void setMecanicosCoche(List<MecanicoCoche> mecanicosCoche) {
        this.mecanicosCoche = mecanicosCoche;
    }

    @Override
    public String toString() {
        return nombre + ", " + apellidos;
    }
}
