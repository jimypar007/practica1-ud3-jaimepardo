package com.jaimepardo.practica1;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "recambio_distribuidor", schema = "tallerjaimepardo", catalog = "")
public class RecambioDistribuidor {
    private int id;
    private int cantidad;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecambioDistribuidor that = (RecambioDistribuidor) o;
        return id == that.id &&
                cantidad == that.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }
}
