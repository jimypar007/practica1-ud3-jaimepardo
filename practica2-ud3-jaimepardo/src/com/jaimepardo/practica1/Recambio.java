package com.jaimepardo.practica1;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Recambio {
    private int id;
    private String componente;
    private Double precio;
    private Byte combustion;
    private Byte electrico;
    private List<RecambioCoche> recambiosCoche;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "componente")
    public String getComponente() {
        return componente;
    }

    public void setComponente(String componente) {
        this.componente = componente;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "combustion")
    public Byte getCombustion() {
        return combustion;
    }

    public void setCombustion(Byte combustion) {
        this.combustion = combustion;
    }

    @Basic
    @Column(name = "electrico")
    public Byte getElectrico() {
        return electrico;
    }

    public void setElectrico(Byte electrico) {
        this.electrico = electrico;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recambio recambio = (Recambio) o;
        return id == recambio.id &&
                Objects.equals(componente, recambio.componente) &&
                Objects.equals(precio, recambio.precio) &&
                Objects.equals(combustion, recambio.combustion) &&
                Objects.equals(electrico, recambio.electrico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, componente, precio, combustion, electrico);
    }

    @OneToMany(mappedBy = "recambio")
    public List<RecambioCoche> getRecambiosCoche() {
        return recambiosCoche;
    }

    public void setRecambiosCoche(List<RecambioCoche> recambiosCoche) {
        this.recambiosCoche = recambiosCoche;
    }

    @Override
    public String toString() {
        return componente + " " + precio + " $";
    }
}
