package com.jaimepardo.practica1;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Factura {
    private int id;
    private double precio;
    private Timestamp fecha;
    private String vendedor;
    private String coche;
    private Cliente cliente;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha")
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "vendedor")
    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    @Basic
    @Column(name = "coche")
    public String getCoche() {
        return coche;
    }

    public void setCoche(String coche) {
        this.coche = coche;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Factura factura = (Factura) o;
        return id == factura.id &&
                Double.compare(factura.precio, precio) == 0 &&
                Objects.equals(fecha, factura.fecha) &&
                Objects.equals(vendedor, factura.vendedor) &&
                Objects.equals(coche, factura.coche);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, precio, fecha, vendedor, coche);
    }

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "Factura " + id  + " :    Cliente: " + cliente.getNombre()+ " " + cliente.getApellidos() + " ----> " + " Coche: " + coche + "    Precio=" + precio+"$";
    }
}
