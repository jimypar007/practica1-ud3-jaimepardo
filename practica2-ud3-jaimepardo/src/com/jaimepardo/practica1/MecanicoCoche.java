package com.jaimepardo.practica1;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "mecanico_coche", schema = "tallerjaimepardo", catalog = "")
public class MecanicoCoche {
    private int id;
    private int cantidad;
    private Coche coche;
    private Mecanico mecanico;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MecanicoCoche that = (MecanicoCoche) o;
        return id == that.id &&
                cantidad == that.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }

    @ManyToOne
    @JoinColumn(name = "id_coche", referencedColumnName = "id")
    public Coche getCoche() {
        return coche;
    }

    public void setCoche(Coche coche) {
        this.coche = coche;
    }

    @ManyToOne
    @JoinColumn(name = "id_mecanico", referencedColumnName = "id")
    public Mecanico getMecanico() {
        return mecanico;
    }

    public void setMecanico(Mecanico mecanico) {
        this.mecanico = mecanico;
    }

    public void nuevaRelacion(Coche coche, Mecanico mecanico) {

        this.setCoche(coche);
        this.setMecanico(mecanico);
        this.setCantidad(1);

    }
}
