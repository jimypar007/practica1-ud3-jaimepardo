package enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el campo de tipo en la tabla de coches.
 * Representan los tipos de coches.
 */
public enum TipoCoche {
    COMBUSTIBLE("Combustible"),
    ELECTRICO("Electrico"),
    HIBRIDO("Hibrido");

    private String valor;

    TipoCoche(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
