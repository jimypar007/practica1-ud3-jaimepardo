package util;

import com.jaimepardo.practica1.*;
import gui.Vista;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

  private static SessionFactory sessionFactory;
  private static Session session;


  /**
   * Crea la factoria de sesiones
   */
  public static void buildSessionFactory(Vista vista) {

    try {
      Configuration configuracion = new Configuration();
      //Cargo el fichero Hibernate.cfg.xml
      configuracion.configure("hibernate.cfg.xml");

      //Indico la clase mapeada con anotaciones
      configuracion.addAnnotatedClass(Coche.class);
      configuracion.addAnnotatedClass(Cliente.class);
      configuracion.addAnnotatedClass(Distribuidor.class);
      configuracion.addAnnotatedClass(Factura.class);
      configuracion.addAnnotatedClass(Mecanico.class);
      configuracion.addAnnotatedClass(Recambio.class);
      configuracion.addAnnotatedClass(RecambioDistribuidor.class);
      configuracion.addAnnotatedClass(RecambioCoche.class);
      configuracion.addAnnotatedClass(MecanicoCoche.class);

      //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
      //Esta clase se usa para gestionar y proveer de acceso a servicios
      StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
              configuracion.getProperties()).build();

      //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
      sessionFactory = configuracion.buildSessionFactory(ssr);

      vista.panel1.setVisible(true);

    } catch (Exception e) {

      vista.panel1.setVisible(false);

    }

  }
	
  /**
   * Abre una nueva sesión
   */
  public static void openSession() {
    session = sessionFactory.openSession();
  }
	
  /**
   * Devuelve la sesión actual
   * @return
   */
  public static Session getCurrentSession() {
	
    if ((session == null) || (!session.isOpen()))
      openSession();

    return session;
  }
	
  /**
   * Cierra Hibernate
   */
  public static void closeSessionFactory() {
	
    if (session != null)
      session.close();
		
    if (sessionFactory != null)
      sessionFactory.close();
  }
}